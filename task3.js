// 1) Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”.

function fizzBuzz(){
    for (let i = 0; i <= 100; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
          console.log("FizzBuzz");
        } else if (i % 3 === 0) {
          console.log("Fizz");
        } else if (i % 5 === 0) {
          console.log("Buzz");
        }
      }
      
}

// Write a program for following condition:
// a) Pass argument (array) to function then return only array element
// b) Sort the array which is filtered in first cycle.
// input: [ [2], 23, ‘dance’, true, [3, 5, 3], [65, 45] ]
// output: [2, 3, 5, 45, 65] sorted order

function filterArray(arg) {
    let myArray = [];
    for (const item of arg) {
      if (Array.isArray(item) && !item.some(isNaN)) {
        myArray.push(...item)
      }
    }
    myArray = [...new Set(myArray)].sort((a, b) => a - b);
    return myArray;
  }
  
  console.log(filterArray([ [2, [2]], 23, 'dance', ['ipsum', 'Lorem text'], true, [3, 5, 3], [65, 45] ]));
  