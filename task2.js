// Write a program that is divisible by 3 and 5
function divisible(num){
    if(num % 3 === 0 && num % 5 === 0){
        console.log(`${num} soni 3 va 5 ga bo'linadi`)
    }
     else{
        console.log(`${num} soni 3 va 5 ga bo'linmaydi`)
     }
}

// Write a program that checks whether given number odd or even.

function oddEven(number){
    if(number % 2 === 0){
        console.log(`${number} soni juft son`)
    }
    else{
        console.log(`${number} soni toq son`)
    }
}

// Write a program that get array as argument and sorts it by order [1,2,3,4...]

let arr = [4, 3, 2, 1];
arr.sort((a, b) => a - b);
console.log(arr);


// 4) Write a program that return unique set of array:
// Input:
// [
// {test: ['a', 'b', 'c', 'd']},
// {test: ['a', 'b', 'c']},
// {test: ['a', 'd']},
// {test: ['a', 'b', 'k', 'e', 'e']},
// ]

// Output: [‘a’, ‘b’, ‘c’, ‘d’, ‘k’, ‘e’]

function unique(arg) {
    let myArray = []
    for (const item of arg) {
      for (const [key, value] of Object.entries(item)) {
        myArray.push(...value)
      }
    }
    myArray = [...new Set(myArray)].sort((a, b) => a - b);
    return myArray;
  }
  
  let arg =
    [
    {test: ['a', 'b', 'c', 'd']},
    {test: ['a', 'b', 'c']},
    {test: ['a', 'd']},
    {test: ['a', 'b', 'k', 'e', 'e']},
    ]
  ;
  console.log(unique(arg));
  
  
  // 5) Write a program that compares two objects by its values and keys, return true if object values are same otherwise return false
  
  let person1 = {
    name: "Sarvar",
    surname: "Aminjonov",
    age: 27,
  };
  let person2 = {
    name: "Sarvar",
    surname: "Aminjonov",
    age: 27,
  };
  
  if (JSON.stringify(person1) === JSON.stringify(person2)) {
    console.log(true);
  } else {
    console.log(false);
  }
  